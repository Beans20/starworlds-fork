package uk.ac.rhul.cs.dice.starworlds.MVC;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;

import uk.ac.rhul.cs.dice.starworlds.connection.INetConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.INetServer;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.INetByteReceiver;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.INetByteSender;
import uk.ac.rhul.cs.dice.starworlds.environment.Universe;

/**
 * A concrete implementation of {@link ViewController} that allows multiple
 * remote Views to connect. This class illustrates the use of a
 * {@link ViewController}.
 * 
 * @author Ben
 *
 */
public class DefaultViewControllerServer extends ViewControllerServer {

	public DefaultViewControllerServer(Universe universe, int port) {
		super(new INetServer(port) {
			@Override
			public INetConnection newConnection(Socket socket) {
				try {
					return new INetConnection(socket, new INetByteSender(
							socket.getOutputStream()), new INetByteReceiver(
							socket.getInputStream())) {
					};
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
		}, universe);
	}

	/**
	 * Prints the sender and the message that was received and then replies.
	 */
	@Override
	public void messageReceived(SocketAddress viewaddr, Object message) {
		System.out.println("View: " + viewaddr + System.lineSeparator()
				+ "    Sent: " + new String((byte[]) message));
		this.send(viewaddr, "Here is some information!".getBytes());
	}
}
