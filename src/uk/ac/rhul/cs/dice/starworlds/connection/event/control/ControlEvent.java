package uk.ac.rhul.cs.dice.starworlds.connection.event.control;

import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.event.AbstractEvent;

public abstract class ControlEvent extends AbstractEvent {

	private static final long serialVersionUID = -1370973174279897190L;

	public ControlEvent() {
		super();
	}

	public ControlEvent(Identifiable origin) {
		super(origin);
	}

}
