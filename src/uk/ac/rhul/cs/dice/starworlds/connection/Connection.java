package uk.ac.rhul.cs.dice.starworlds.connection;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.Receiver;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.Sender;

public interface Connection<T> extends Sender<T>, Receiver<T> {

}
