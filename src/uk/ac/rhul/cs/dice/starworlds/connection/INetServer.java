package uk.ac.rhul.cs.dice.starworlds.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class INetServer<T> {

	protected ServerSocket socket;
	protected Map<SocketAddress, INetConnection<T>> connections = new HashMap<>();
	protected boolean open = true;

	public INetServer(int port) {
		init(port);
	}

	protected void init(int port) {
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("SERVER: " + socket + " Waiting for connections...");
		Thread socketthread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (open) {
					try {
						Socket socket = INetServer.this.socket.accept();
						System.out.println("Accepted Connection: " + socket);
						INetConnection<T> connection = newConnection(socket);
						if (processConnection(connection)) {
							connections.put(socket.getRemoteSocketAddress(), connection);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		socketthread.start();
	}

	protected abstract boolean processConnection(INetConnection<T> connection);

	protected abstract INetConnection<T> newConnection(Socket socket);

	protected INetConnection<T> connect(String host, Integer port) throws IOException {
		System.out.println(this + " INITIATING COMMUNICATION WITH: " + host + ":" + port);
		Socket socket = new Socket(host, port);
		INetConnection<T> connection = newConnection(socket);
		connections.put(socket.getRemoteSocketAddress(), connection);
		return connection;
	}

	public Set<SocketAddress> getClientAddresses() {
		return Collections.unmodifiableSet(connections.keySet());
	}

	public INetConnection<T> getConnection(SocketAddress addr) {
		return this.connections.get(addr);
	}

	public Collection<INetConnection<T>> getConnections() {
		return this.connections.values();
	}

	@Override
	public String toString() {
		return socket.toString();
	}

	public Integer getLocalPort() {
		return socket.getLocalPort();
	}
}
