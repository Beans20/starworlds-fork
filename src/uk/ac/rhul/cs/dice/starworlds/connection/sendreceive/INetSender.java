package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.io.OutputStream;

public interface INetSender<T> extends Sender<T> {

	public void setOutputStream(OutputStream out);

	public OutputStream getOutputStream();

}
