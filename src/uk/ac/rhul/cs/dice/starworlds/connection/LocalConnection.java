package uk.ac.rhul.cs.dice.starworlds.connection;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.LocalReceiver;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.LocalSender;

public abstract class LocalConnection<T> extends AbstractConnection<T> {

	protected LocalConnection<T> remote;

	public LocalConnection(LocalSender<T> sender, LocalReceiver<T> receiver) {
		super(sender, receiver);
	}

	public LocalConnection(LocalConnection<T> remote, LocalSender<T> sender, LocalReceiver<T> receiver) {
		super(sender, receiver);
		this.remote = remote;
		this.remote.setRemote(remote);
		sender.setReceiver(remote.getReceiver());
		remote.getSender().setReceiver(receiver);
	}

	public void setRemote(LocalConnection<T> remote) {
		this.remote = remote;
	}

	@Override
	public LocalReceiver<T> getReceiver() {
		return (LocalReceiver<T>) super.getReceiver();
	}

	@Override
	public LocalSender<T> getSender() {
		return (LocalSender<T>) super.getSender();
	}

}
