package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public abstract class AbstractINetSender<T> implements INetSender<T> {

	protected OutputStream out;

	public AbstractINetSender() {
	}

	public AbstractINetSender(OutputStream out) {
		setOutputStream(out);
	}

	public AbstractINetSender(Socket socket) throws IOException {
		setOutputStream(socket.getOutputStream());
	}

	@Override
	public void setOutputStream(OutputStream out) {
		this.out = out;
	}

	@Override
	public OutputStream getOutputStream() {
		return this.out;
	}
}
