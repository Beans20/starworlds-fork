package uk.ac.rhul.cs.dice.starworlds.connection;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.Receiver;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.Sender;

public abstract class AbstractConnection<T> implements Connection<T> {

	private Sender<T> sender;
	private Receiver<T> receiver;

	public AbstractConnection(Sender<T> sender, Receiver<T> receiver) {
		super();
		this.sender = sender;
		this.receiver = receiver;
	}

	public Sender<T> getSender() {
		return sender;
	}

	public Receiver<T> getReceiver() {
		return receiver;
	}

	@Override
	public void send(T message) {
		sender.send(message);
	}

	@Override
	public T receive() {
		return receiver.receive();
	}

	@Override
	public boolean avaliable() {
		return receiver.avaliable();
	}
}
