package uk.ac.rhul.cs.dice.starworlds.connection.event;

import java.io.IOException;
import java.net.Socket;

import uk.ac.rhul.cs.dice.starworlds.connection.INetConnection;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class INetEventConnection extends INetConnection<Event> {

	public INetEventConnection(Socket socket) throws IOException {
		super(socket, new INetEventSender(socket), new INetEventReceiver(socket));
	}

	@Override
	public String toString() {
		return "Remote Connection";
	}

}
