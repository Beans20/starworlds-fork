package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

public interface Sender<T> {

	public void send(T message);

}
