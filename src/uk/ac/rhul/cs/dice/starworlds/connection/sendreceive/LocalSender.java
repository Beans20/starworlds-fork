package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

public class LocalSender<T> implements Sender<T> {

	private LocalReceiver<T> receiver;

	@Override
	public void send(T message) {
		receiver.push(message);
	}

	public void setReceiver(LocalReceiver<T> receiver) {
		this.receiver = receiver;
	}

}
