package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.io.InputStream;

public interface INetReceiver<T> extends Receiver<T> {

	public void setInputStream(InputStream in);

	public InputStream getInputStream();
}
