package uk.ac.rhul.cs.dice.starworlds.connection.event;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.connection.INetConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.INetServer;
import uk.ac.rhul.cs.dice.starworlds.connection.event.control.InitialisationEvent;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.EnvironmentEventPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.EnvironmentRelation;
import uk.ac.rhul.cs.dice.starworlds.event.Event;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public class INetEventServer<E extends Enum<E> & EnvironmentRelation<E>> extends INetServer<Event> {

	private Map<String, EnvironmentEventPortal<E>.ConnectionInitialiser> initialisers;

	// private Thread listenerThread;
	// private INetEventListener listener;

	public INetEventServer(int port) {
		super(port);
		initialisers = new HashMap<>();
	}

	public void addInitialiser(EnvironmentEventPortal<E>.ConnectionInitialiser initialiser) {
		this.initialisers.put(initialiser.getEnvironmentAppearance().getId(), initialiser);
	}

	public void removeInitialiser(EnvironmentEventPortal<E>.ConnectionInitialiser initialiser) {
		this.initialisers.remove(initialiser.getEnvironmentAppearance().getId());
	}

	@Override
	public synchronized INetEventConnection getConnection(SocketAddress addr) {
		return (INetEventConnection) super.getConnection(addr);
	}

	@Override
	public synchronized INetEventConnection connect(String addr, Integer port) throws IOException {
		return (INetEventConnection) super.connect(addr, port);
	}

	@Override
	protected INetEventConnection newConnection(Socket socket) {
		try {
			return new INetEventConnection(socket);
		} catch (IOException e) {
			throw new StarWorldsRuntimeException("Failed to create connection", e);
		}
	}

	@Override
	protected boolean processConnection(INetConnection<Event> connection) {
		Event event = connection.receive();
		try {
			InitialisationEvent<?> init = (InitialisationEvent<?>) event;
			return this.initialisers.get(init.getRemote()).incomingConnection(init, (INetEventConnection) connection);
		} catch (ClassCastException e) {
			throw new StarWorldsRuntimeException("Invalid initialisation event: " + event, e);
		} catch (NullPointerException e) {
			throw new StarWorldsRuntimeException(
					"Invalid initialisation event: " + event + " no initialiser was found", e);
		}
	}
}
