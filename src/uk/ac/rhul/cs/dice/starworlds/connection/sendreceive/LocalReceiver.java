package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.util.LinkedList;
import java.util.Queue;

public class LocalReceiver<T> implements Receiver<T> {

	private Queue<T> input = new LinkedList<>();

	@Override
	public T receive() {
		return pull();
	}

	public void push(T in) {
		input.add(in);
	}

	public T pull() {
		return input.poll();
	}

	@Override
	public boolean avaliable() {
		return !input.isEmpty();
	}
}
