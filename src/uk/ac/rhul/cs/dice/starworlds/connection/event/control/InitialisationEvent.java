package uk.ac.rhul.cs.dice.starworlds.connection.event.control;

import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.EnvironmentRelation;

public class InitialisationEvent<E extends Enum<E> & EnvironmentRelation<E>> extends ControlEvent {
	private static final long serialVersionUID = 569702086310221713L;

	E relation;
	String remote;

	public InitialisationEvent(Identifiable origin, String remote, E relation) {
		super(origin);
		this.relation = relation;
		this.remote = remote;
	}

	public E getRelation() {
		return relation;
	}

	public String getRemote() {
		return remote;
	}
}