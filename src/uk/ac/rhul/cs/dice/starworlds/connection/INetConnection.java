package uk.ac.rhul.cs.dice.starworlds.connection;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.INetReceiver;
import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.INetSender;

public class INetConnection<T> extends AbstractConnection<T> implements INetSender<T>, INetReceiver<T> {

	private Socket socket;

	public INetConnection(Socket socket, INetSender<T> sender, INetReceiver<T> receiver) {
		super(sender, receiver);
		this.socket = socket;
	}

	public SocketAddress getRemoteSocketAddress() {
		return this.socket.getRemoteSocketAddress();
	}

	public Socket getSocket() {
		return this.socket;
	}

	@Override
	public INetReceiver<T> getReceiver() {
		return (INetReceiver<T>) super.getReceiver();
	}

	@Override
	public INetSender<T> getSender() {
		return (INetSender<T>) super.getSender();
	}

	@Override
	public InputStream getInputStream() {
		return getReceiver().getInputStream();
	}

	@Override
	public OutputStream getOutputStream() {
		return getSender().getOutputStream();
	}

	@Override
	public void setInputStream(InputStream in) {
		getReceiver().setInputStream(in);
	}

	@Override
	public void setOutputStream(OutputStream out) {
		getSender().setOutputStream(out);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ": " + this.socket.toString();
	}
}
