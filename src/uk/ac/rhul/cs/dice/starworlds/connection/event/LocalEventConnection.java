package uk.ac.rhul.cs.dice.starworlds.connection.event;

import uk.ac.rhul.cs.dice.starworlds.connection.LocalConnection;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class LocalEventConnection extends LocalConnection<Event> {

	public LocalEventConnection(LocalEventConnection remote, LocalEventSender sender, LocalEventReceiver receiver) {
		super(remote, sender, receiver);
	}

	public LocalEventConnection(LocalEventSender sender, LocalEventReceiver receiver) {
		super(sender, receiver);
	}

	@Override
	public void setRemote(LocalConnection<Event> remote) {
		super.setRemote((LocalEventConnection) remote);
	}

	@Override
	public String toString() {
		return "Local Connection";
	}
}
