package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.net.Socket;

/**
 * Receiver functional interface. Used as a facade to receive data from some input (e.g. a {@link Socket}).
 * 
 * @author Ben Wilkins
 *
 * @param <T>
 *            type of data being received
 */
public interface Receiver<T> {

	/**
	 * Reads input and returns it.
	 * 
	 * @return input
	 */
	public T receive();

	/**
	 * Is there any input to read.
	 * 
	 * @return true if there is, false otherwise.
	 */
	public boolean avaliable();
}
