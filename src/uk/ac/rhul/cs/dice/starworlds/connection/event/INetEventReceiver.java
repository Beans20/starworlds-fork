package uk.ac.rhul.cs.dice.starworlds.connection.event;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.AbstractINetReceiver;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class INetEventReceiver extends AbstractINetReceiver<Event> {

	public INetEventReceiver(Socket socket) throws IOException {
		super(new ObjectInputStream(socket.getInputStream()));
	}

	@Override
	public Event receive() {
		try {
			return (Event) getInputStream().readObject();
		} catch (ClassNotFoundException | ClassCastException | IOException e) {
			System.err.println(this.getClass().getSimpleName() + ": Invalid data received");
		}
		return null;
	}

	@Override
	public ObjectInputStream getInputStream() {
		return (ObjectInputStream) super.getInputStream();
	}


}
