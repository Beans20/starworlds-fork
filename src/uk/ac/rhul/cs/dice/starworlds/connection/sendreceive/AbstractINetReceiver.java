package uk.ac.rhul.cs.dice.starworlds.connection.sendreceive;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public abstract class AbstractINetReceiver<T> implements INetReceiver<T> {

	protected InputStream in;

	public AbstractINetReceiver(Socket socket) throws IOException {
		this.in = socket.getInputStream();
	}

	public AbstractINetReceiver(InputStream in) {
		setInputStream(in);
	}

	@Override
	public void setInputStream(InputStream in) {
		this.in = in;
	}

	@Override
	public InputStream getInputStream() {
		return this.in;
	}

	@Override
	public boolean avaliable() {
		try {
			return this.in.available() > 0;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
