package uk.ac.rhul.cs.dice.starworlds.connection.event;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import uk.ac.rhul.cs.dice.starworlds.connection.sendreceive.AbstractINetSender;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class INetEventSender extends AbstractINetSender<Event> {

	public INetEventSender(Socket socket) throws IOException {
		super(new ObjectOutputStream(socket.getOutputStream()));
	}

	@Override
	public void send(Event event) {
		try {
			getOutputStream().writeObject(event);
			getOutputStream().flush();
			System.out.println(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ObjectOutputStream getOutputStream() {
		return (ObjectOutputStream) super.getOutputStream();
	}

}
