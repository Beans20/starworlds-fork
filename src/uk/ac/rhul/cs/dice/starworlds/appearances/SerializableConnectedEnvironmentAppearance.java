package uk.ac.rhul.cs.dice.starworlds.appearances;

import java.util.Collection;
import java.util.stream.Collectors;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableEnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializablePortalAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractConnectedEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.utils.StringUtils;

public class SerializableConnectedEnvironmentAppearance extends SerializableEnvironmentAppearance {

	private static final long serialVersionUID = 5574691590029145904L;

	private Collection<SerializablePortalAppearance> portals;

	public SerializableConnectedEnvironmentAppearance(AbstractConnectedEnvironment<?, ?> environment) {
		super(environment);
		portals = environment.getState().getPortals().stream().map(this::getSerializablePortalAppearance)
				.collect(Collectors.toList());
	}

	@Override
	public String represent() {
		return super.represent() + " : " + StringUtils.collectionToStringSingle(portals);
	}

	private SerializablePortalAppearance getSerializablePortalAppearance(AbstractPortal portal) {
		return (SerializablePortalAppearance) portal.getAppearance().getSerializable();
	}

}
