package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableAppearance;

public class ConnectedEnvironmentAppearance extends EnvironmentAppearance {

	/**
	 * Constructor.
	 *
	 */
	public ConnectedEnvironmentAppearance() {
		super();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public SerializableAppearance getSerializable() {
		// TODO Auto-generated method stub
		return super.getSerializable();
	}
}
