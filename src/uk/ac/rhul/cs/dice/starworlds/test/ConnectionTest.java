package uk.ac.rhul.cs.dice.starworlds.test;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AmbientPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimpleAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimpleConnectedPhysics;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimpleConnectedUniverse;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.DefaultEnvironmentRelation;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;
import uk.ac.rhul.cs.dice.starworlds.perception.CommunicationPerception;

public class ConnectionTest {

	private static String HOST = "localhost";
	private static Integer PORT1 = 10001;
	private static Integer PORT2 = 10000;

	public static void main(String[] args) throws UnknownHostException {
		// INetEventServer<DefaultEnvironmentRelation> server1 = new INetEventServer<DefaultEnvironmentRelation>(PORT1);
		// INetEventServer<DefaultEnvironmentRelation> server2 = new INetEventServer<DefaultEnvironmentRelation>(PORT2);

		Set<AbstractPortal> portals1 = new HashSet<>();
		AmbientPortal portal1 = new AmbientPortal("portal1");
		portals1.add(portal1);
		Set<AbstractPortal> portals2 = new HashSet<>();
		AmbientPortal portal2 = new AmbientPortal("portal1");
		portals2.add(portal2);

		SimpleConnectedUniverse env1 = new SimpleConnectedUniverse("env1", new SimpleAmbient(null, null, null, null, portals1),
				new SimpleConnectedPhysics<>(), new SensorSubscriptionHandler());
		SimpleConnectedUniverse env2 = new SimpleConnectedUniverse("env2", new SimpleAmbient(null, null, null, null, portals2),
				new SimpleConnectedPhysics<>(), new SensorSubscriptionHandler());

		env1.connect(env2, DefaultEnvironmentRelation.NEIGHBOUR);
		env1.connectPortal("portal1", "env2");
		env2.connectPortal("portal1", "env1");

		System.out.println(env1.getPortal().getConnection("env2"));
		System.out.println(env2.getPortal().getConnection("env1"));

		portal1.pushEvent(new CommunicationPerception("Hello! from portal1"));
		System.out.println(portal2.pullEvents());
		
		
		portal2.pushEvent(new CommunicationPerception("Hello! from portal2"));
		System.out.println(portal1.pullEvents());

		// System.out.println(env1.getSubscriber());
	}
}
