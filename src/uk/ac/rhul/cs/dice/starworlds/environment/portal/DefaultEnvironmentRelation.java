package uk.ac.rhul.cs.dice.starworlds.environment.portal;

public enum DefaultEnvironmentRelation implements EnvironmentRelation<DefaultEnvironmentRelation> {

	SUPER() {
		@Override
		public DefaultEnvironmentRelation inverse() {
			return SUB;
		}
	},
	SUB() {
		@Override
		public DefaultEnvironmentRelation inverse() {
			return SUPER;
		}
	},
	NEIGHBOUR() {
		@Override
		public DefaultEnvironmentRelation inverse() {
			return NEIGHBOUR;
		}
	};

}
