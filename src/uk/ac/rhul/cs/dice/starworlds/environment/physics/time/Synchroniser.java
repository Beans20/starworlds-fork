package uk.ac.rhul.cs.dice.starworlds.environment.physics.time;

import uk.ac.rhul.cs.dice.starworlds.environment.AbstractConnectedEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.Simulator;
import uk.ac.rhul.cs.dice.starworlds.environment.interaction.event.EventListener;

public interface Synchroniser extends EventListener, Simulator {

	public AbstractConnectedEnvironment getEnvironment();

	public void runActors();

	public void propagateActions();

	public void executeActions();
}
