package uk.ac.rhul.cs.dice.starworlds.environment.interaction.event;

import uk.ac.rhul.cs.dice.starworlds.event.Event;

public interface EventListener {

	public abstract void update(Object origin, Event event);

}
