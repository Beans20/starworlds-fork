package uk.ac.rhul.cs.dice.starworlds.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.connection.AbstractConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.Connection;
import uk.ac.rhul.cs.dice.starworlds.connection.event.INetEventConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.event.LocalEventConnection;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AmbientPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.Portal;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class EnvironmentConnection implements Connection<Event> {

	private AbstractConnection<Event> connection;
	private Map<String, Queue<Event>> portalEvents;
	private Queue<Event> otherEvents;
	private Appearance local;
	private Appearance remote;

	public EnvironmentConnection(INetEventConnection connection) {
		this.connection = connection;
		portalEvents = new HashMap<>();
		otherEvents = new LinkedList<>();
	}

	public EnvironmentConnection(LocalEventConnection connection) {
		this.connection = connection;
		portalEvents = new HashMap<>();
		otherEvents = new LinkedList<>();
	}

	public void addPortal(AmbientPortal portal) {
		portalEvents.put(portal.getId(), new LinkedList<Event>());
	}

	public void removePortal(Portal portal) {
		portalEvents.remove(portal.getId());
	}

	public static class PortalEvent implements Event {

		private static final long serialVersionUID = -4098990140902915948L;

		private String portal;
		private Event event;

		public PortalEvent(String portal, Event event) {
			super();
			this.portal = portal;
			this.event = event;
		}

		@Override
		public String getId() {
			return "";
		}

		@Override
		public void setId(String id) {
			throw new UnsupportedOperationException("Cannot set the id of a " + this.getClass().getSimpleName());
		}

		@Override
		public String getOrigin() {
			return portal;
		}

		public Event getEvent() {
			return event;
		}
	}

	/**
	 * Send an event via this connection to its associated {@link Portal}. The {@link Event} will be wrapped in a
	 * {@link PortalEvent}.
	 * 
	 * @param portal
	 *            : the sending {@link Portal}
	 * @param event
	 *            : to send
	 */
	public void send(Portal portal, Event event) {
		this.send(new PortalEvent(portal.getId(), event));
	}

	/**
	 * Pulls the most recent {@link Event}s sent to the given {@link Portal}. Calling this has the side effect of
	 * pulling all {@link Event}s from the {@link Connection} and assigning them to a buffer (for all {@link Portal}).
	 * 
	 * @param portal
	 *            : to receive
	 * @return the {@link Event}
	 */
	public Collection<Event> receive(AbstractPortal portal) {
		while (this.connection.avaliable()) {
			Event e = this.connection.receive();
			if (e instanceof PortalEvent) {
				PortalEvent pe = (PortalEvent) e;
				portalEvents.get(pe.getOrigin()).offer(pe.getEvent());
			} else {
				otherEvents.offer(e);
			}
		}
		return new ArrayList<>(this.portalEvents.get(portal.getId()));
	}

	@Override
	public void send(Event message) {
		this.connection.send(message);
	}

	@Override
	public Event receive() {
		return connection.receive();
	}

	@Override
	public boolean avaliable() {
		return connection.avaliable();
	}

	public String getRemoteID() {
		return this.getRemoteAppearance().getId();
	}

	public Appearance getRemoteAppearance() {
		return this.remote;
	}

	public void setRemoteAppearance(Appearance appearance) {
		this.remote = appearance;
	}

	public String getLocalID() {
		return this.getLocalAppearance().getId();
	}

	public Appearance getLocalAppearance() {
		return this.local;
	}

	public void setLocalAppearance(Appearance appearance) {
		this.local = appearance;
	}

	@Override
	public String toString() {
		return this.connection.toString() + " : " + this.getRemoteID();
	}
}
