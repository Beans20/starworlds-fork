package uk.ac.rhul.cs.dice.starworlds.environment.physics;

import uk.ac.rhul.cs.dice.starworlds.appearances.EnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.time.EnvironmentSynchroniser;

public abstract class AbstractConnectedPhysics<E extends Environment<E>> extends AbstractPhysics<E> {

	protected EnvironmentSynchroniser synchroniser;

	public AbstractConnectedPhysics() {
		super();
	}

	@Override
	public void simulate() {
		synchroniser.simulate();
	}

	@Override
	public void run() {
		this.simulate();
	}

	public EnvironmentSynchroniser getSynchroniser() {
		return synchroniser;
	}

	public void addSynchroniser(EnvironmentAppearance environment) {
		synchroniser.addSynchroniser(environment);
	}
}
