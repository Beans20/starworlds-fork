package uk.ac.rhul.cs.dice.starworlds.environment.portal;

public interface EnvironmentRelation<E extends Enum<E>> {

	public E inverse();

}
