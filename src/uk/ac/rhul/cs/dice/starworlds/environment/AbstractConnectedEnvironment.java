package uk.ac.rhul.cs.dice.starworlds.environment;

import uk.ac.rhul.cs.dice.starworlds.appearances.ConnectedEnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.connection.event.INetEventServer;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AmbientPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.AbstractPhysics;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.EnvironmentEventPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.EnvironmentRelation;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;

public abstract class AbstractConnectedEnvironment<R extends Enum<R> & EnvironmentRelation<R>, E extends Environment<E>>
		extends AbstractEnvironment<E> {

	private EnvironmentEventPortal<R> portal;

	public AbstractConnectedEnvironment(String id, Class<R> relations, Class<E> cls, AbstractAmbient ambient,
			AbstractPhysics<E> physics, SensorSubscriptionHandler subscriptionHandler,
			ConnectedEnvironmentAppearance appearance) {
		super(id, cls, ambient, physics, subscriptionHandler, appearance);
		this.portal = new EnvironmentEventPortal<>(this, relations);
	}

	public AbstractConnectedEnvironment(String id, Class<R> relations, INetEventServer<R> server, Class<E> cls,
			AbstractAmbient ambient, AbstractPhysics<E> physics, SensorSubscriptionHandler subscriptionHandler,
			ConnectedEnvironmentAppearance appearance) {
		super(id, cls, ambient, physics, subscriptionHandler, appearance);
		this.portal = new EnvironmentEventPortal<>(this, server, relations);
	}

	public boolean connect(String remote, R relation, String addr, Integer port) {
		return this.portal.connect(remote, relation, addr, port);
	}

	public boolean connect(AbstractConnectedEnvironment<R, ?> local, R relation) {
		return this.portal.connect(local.portal, relation);
	}

	public EnvironmentEventPortal<R> getPortal() {
		return portal;
	}

	public void connectPortal(String portal, String remoteEnvironment) {
		AmbientPortal port = ((AmbientPortal) this.getState().getPortal(portal));
		port.setConnection(this.portal.getConnection(remoteEnvironment));
	}
}
