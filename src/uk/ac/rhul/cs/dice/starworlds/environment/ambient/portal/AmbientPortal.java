package uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.appearances.PortalAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.EnvironmentConnection;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

public class AmbientPortal extends AbstractPortal {

	private EnvironmentConnection connection;

	public <E extends Event> AmbientPortal(String id) {
		super(id, new PortalAppearance());
	}

	public <E extends Event> AmbientPortal(String id, PortalAppearance appearance) {
		super(id, appearance);
		this.getAppearance().manifest(this);
	}

	public void setConnection(EnvironmentConnection connection) {
		this.connection = connection;
		this.connection.addPortal(this);
	}

	public Collection<Event> pullEvents() {
		return connection.receive(this);
	}

	public void pushEvent(Event event) {
		connection.send(this, event);
	}

	public void pushEvents(Collection<Event> events) {
		for (Event e : events) {
			connection.send(this, e);
		}
	}
}
