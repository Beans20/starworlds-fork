package uk.ac.rhul.cs.dice.starworlds.environment.portal;

import java.io.IOException;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.connection.event.INetEventConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.event.INetEventServer;
import uk.ac.rhul.cs.dice.starworlds.connection.event.LocalEventConnection;
import uk.ac.rhul.cs.dice.starworlds.connection.event.LocalEventReceiver;
import uk.ac.rhul.cs.dice.starworlds.connection.event.LocalEventSender;
import uk.ac.rhul.cs.dice.starworlds.connection.event.control.InitialisationEvent;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.EnvironmentConnection;
import uk.ac.rhul.cs.dice.starworlds.event.Event;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public class EnvironmentEventPortal<E extends Enum<E> & EnvironmentRelation<E>> extends AbstractEnvironmentPortal<E> {

	private INetEventServer<E> server;
	private ConnectionInitialiser initialiser;
	private Appearance local;

	public EnvironmentEventPortal(Environment<?> local, INetEventServer<E> server, Class<E> relations) {
		super(relations);
		this.local = local.getAppearance();
		this.initialiser = new ConnectionInitialiser();
		this.server = server;
		this.server.addInitialiser(initialiser);
	}

	public EnvironmentEventPortal(Environment<?> local, Class<E> relations) {
		super(relations);
		this.local = local.getAppearance();
		this.initialiser = null;
		this.server = null;
	}

	public boolean connect(EnvironmentEventPortal<E> remote, E relation) {
		LocalEventConnection lc = new LocalEventConnection(new LocalEventSender(), new LocalEventReceiver());
		LocalEventConnection rc = new LocalEventConnection(new LocalEventSender(), new LocalEventReceiver());
		lc.setRemote(rc);
		rc.setRemote(lc);
		rc.getSender().setReceiver(lc.getReceiver());
		lc.getSender().setReceiver(rc.getReceiver());

		EnvironmentConnection clocal = new EnvironmentConnection(lc);
		EnvironmentConnection cremote = new EnvironmentConnection(rc);
		clocal.setLocalAppearance(this.local);
		cremote.setLocalAppearance(remote.local);
		clocal.setRemoteAppearance(remote.local);
		cremote.setRemoteAppearance(this.local);

		remote.addConnection(relation.inverse(), cremote);
		this.addConnection(relation, clocal);
		return true;
	}

	public boolean connect(String remote, E relation, String addr, Integer port) {
		if (server != null) {
			try {
				return initialiser.connect(remote, relation, addr, port);
			} catch (IOException e) {
				throw new StarWorldsRuntimeException("Failed to connect", e);
			}
		} else {
			throw new StarWorldsRuntimeException("No " + INetEventServer.class.getSimpleName()
					+ " has been set up for this portal, only local connections can be made");
		}
	}

	public class ConnectionInitialiser {

		public boolean incomingConnection(InitialisationEvent<?> event, INetEventConnection connection) {
			EnvironmentConnection econnection = new EnvironmentConnection(connection);
			econnection.setRemoteAppearance((Appearance) event.getOrigin());
			econnection.setLocalAppearance(getEnvironmentAppearance());
			E relation = Enum.valueOf(EnvironmentEventPortal.this.relations, event.getRelation().toString());
			EnvironmentEventPortal.this.addConnection(relation.inverse(), econnection);
			connection.send(new InitialisationEvent<>(getEnvironmentAppearance().getSerializable(), event.getOrigin()
					.getId(), relation.inverse()));
			System.out.println(this.getEnvironmentAppearance().getId() + " " + relation.inverse() + " "
					+ event.getOrigin().getId());
			return true;
		}

		public boolean connect(String remote, E relation, String addr, Integer port) throws IOException {
			INetEventConnection connection = EnvironmentEventPortal.this.server.connect(addr, port);
			connection.send(new InitialisationEvent<E>(getEnvironmentAppearance().getSerializable(), remote, relation));
			Event event = connection.receive();
			try {
				InitialisationEvent<?> init = (InitialisationEvent<?>) event;
				if (init.getRelation().inverse().equals(relation) && init.getOrigin().getId().equals(remote)) {
					// success!
					this.addConnection(init, connection);
					System.out.println(this.getEnvironmentAppearance().getId() + " " + relation + " " + remote);
					return true;
				}
			} catch (ClassCastException e) {
				throw new StarWorldsRuntimeException("Invalid initialisation event: " + event, e);
			} catch (NullPointerException e) {
				throw new StarWorldsRuntimeException("Invalid initialisation event: " + event
						+ " no initialiser was found", e);
			}
			return false;
		}

		private void addConnection(InitialisationEvent<?> event, INetEventConnection connection) {
			EnvironmentConnection econnection = new EnvironmentConnection(connection);
			econnection.setRemoteAppearance((Appearance) event.getOrigin());
			econnection.setLocalAppearance(getEnvironmentAppearance());
			E relation = Enum.valueOf(EnvironmentEventPortal.this.relations, event.getRelation().toString());
			EnvironmentEventPortal.this.addConnection(relation, econnection);
		}

		public Appearance getEnvironmentAppearance() {
			return EnvironmentEventPortal.this.local;
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName() + " " + this.getEnvironmentAppearance().getId();
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.local.getId()).append(":")
				.append(this.getClass().getSimpleName()).append(System.lineSeparator());
		this.connections
				.entrySet()
				.stream()
				.forEach(
						e -> builder.append("   ").append(e.getKey()).append(e.getValue().values())
								.append(System.lineSeparator()));
		return builder.toString();
	}
}
