package uk.ac.rhul.cs.dice.starworlds.environment.concrete;

import uk.ac.rhul.cs.dice.starworlds.appearances.ConnectedEnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractConnectedEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.portal.DefaultEnvironmentRelation;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;

public class SimpleConnectedUniverse extends
		AbstractConnectedEnvironment<DefaultEnvironmentRelation, SimpleConnectedUniverse> {

	public SimpleConnectedUniverse(String id, AbstractAmbient ambient,
			SimpleConnectedPhysics<SimpleConnectedUniverse> physics, SensorSubscriptionHandler subscriptionHandler) {
		super(id, DefaultEnvironmentRelation.class, SimpleConnectedUniverse.class, ambient, physics,
				subscriptionHandler, new ConnectedEnvironmentAppearance());
	}

	@Override
	public void postInitialisation() {
	}

	@Override
	public Boolean isSimple() {
		return true;
	}

}
