package uk.ac.rhul.cs.dice.starworlds.environment.concrete;

import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.AbstractConnectedPhysics;

public class SimpleConnectedPhysics<E extends Environment<E>> extends AbstractConnectedPhysics<E> {

	public SimpleConnectedPhysics() {
		super();
	}

	@Override
	public void cycleAddition() {
	}
}
