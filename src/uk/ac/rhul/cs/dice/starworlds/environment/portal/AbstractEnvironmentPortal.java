package uk.ac.rhul.cs.dice.starworlds.environment.portal;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.environment.EnvironmentConnection;

public abstract class AbstractEnvironmentPortal<E extends Enum<E> & EnvironmentRelation<E>> {

	protected EnumMap<E, Map<String, EnvironmentConnection>> connections;
	protected Class<E> relations;

	public AbstractEnvironmentPortal(Class<E> relations) {
		this.relations = relations;
		connections = new EnumMap<>(relations);
		for (E e : relations.getEnumConstants()) {
			connections.put(e, new HashMap<>());
		}
	}

	public void addConnection(E key, EnvironmentConnection connection) {
		connections.get(key).put(connection.getRemoteID(), connection);
	}

	public EnvironmentConnection getConnection(String remote) {
		Iterator<Map<String, EnvironmentConnection>> iter = this.connections.values().iterator();
		EnvironmentConnection result = null;
		while ((iter.hasNext() && (result = iter.next().get(remote)) == null)) {
		}
		return result;
	}

	public EnvironmentConnection getConnection(E key, String remote) {
		return connections.get(key).get(remote);
	}

	public Collection<EnvironmentConnection> getConnections(E key) {
		return connections.get(key).values();
	}

	public EnumMap<E, Map<String, EnvironmentConnection>> getConnections() {
		return connections;
	}

	public boolean isEmpty(E key) {
		return this.connections.get(key).isEmpty();
	}

	public boolean exists(String remote) {
		for (Map<String, EnvironmentConnection> m : connections.values()) {
			if (m.containsKey(remote)) {
				return true;
			}
		}
		return false;
	}

	public boolean exists(E key, String remote) {
		return connections.get(key).containsKey(remote);
	}
}
